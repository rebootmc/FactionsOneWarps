/**
 *
 * FactionWarps - Copyright 2016 P.Eduard All Rights Reserved
 */
package me.eduproard.FactionWarps;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.event.FPlayerLeaveEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.event.FactionRenameEvent;

import net.md_5.bungee.api.ChatColor;

public class envEvents implements Listener {
	
	private Main Main;
	public envEvents(Main Plugin) {
		Main = Plugin;
		
		Main.getServer().getPluginManager().registerEvents(this, Main);
	}
	@EventHandler
	public void on(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		if(e.getTo().getBlockX() != e.getFrom().getBlockX() || e.getTo().getBlockZ() != e.getFrom().getBlockZ()) {
			if(Main.WarmupMap.containsKey(p.getName())) {
				Main.WarmupMap.get(p.getName()).cancel();
				Main.WarmupMap.remove(p.getName());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("TP-CANCEL")));
			}
		}
	}
	@EventHandler
	public void on(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		if(Main.CooldownMap.containsKey(p.getName())) {
			Main.CooldownMap.remove(p.getName());
		}
		if(Main.WarmupMap.containsKey(p.getName())) {
			Main.WarmupMap.get(p.getName()).cancel();
			Main.WarmupMap.remove(p.getName());
		}
		if(Main.ConfirmationList.contains(p.getName())) {
			Main.ConfirmationList.remove(p.getName());
		}
	}
	@EventHandler
	public void on(FactionDisbandEvent e) {
		String Faction = e.getFaction().getTag();
		
		if(Main.GetStorage().GetData().contains("Factions." + Faction)) {
			Main.GetStorage().GetData().set("Factions." + Faction, null);
			Main.GetStorage().SaveData();
		}
	}
	@EventHandler
	public void on(FactionRenameEvent e) {
		String OldTag = e.getOldFactionTag();
		String NewTag = e.getFactionTag();
		
		if(Main.GetStorage().GetData().contains("Factions." + OldTag)) {
			for(String Warp : Main.GetStorage().GetData().getConfigurationSection("Factions." + OldTag).getKeys(false)) {
				String WarpLocation = Main.GetStorage().GetData().getString("Factions." + OldTag + ".Location");
				String WarpPassword = null;
				if(Main.GetStorage().GetData().contains("Factions." + OldTag + ".Password")) {
					WarpPassword = Main.GetStorage().GetData().getString("Factions." + OldTag + ".Password");
				}
				Main.GetStorage().GetData().set("Factions." + NewTag + "." + Warp + ".Location", WarpLocation);
				Main.GetStorage().GetData().set("Factions." + NewTag + "." + Warp + ".Password", WarpPassword);
				Main.GetStorage().SaveData();
			}
		}
	}
	@EventHandler
	public void on(FPlayerLeaveEvent e) {
		FPlayer Player = e.getFPlayer();
		Player p = Player.getPlayer();
		
		if(Main.CooldownMap.containsKey(Player.getName())) {
			Main.CooldownMap.remove(Player.getName());
		}
		if(Main.WarmupMap.containsKey(Player.getName())) {
			Main.WarmupMap.get(Player.getName()).cancel();
			Main.WarmupMap.remove(Player.getName());
		}
		if(Main.ConfirmationList.contains(Player.getName())) {
			Main.ConfirmationList.remove(Player.getName());
		}
	}
}