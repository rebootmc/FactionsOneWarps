/**
 *
 * FactionWarps - Copyright 2016 P.Eduard All Rights Reserved
 */
package me.eduproard.FactionWarps;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FactionStorage {
	
	private FileConfiguration data;
	private File dfile;

	public FactionStorage() {}
	
	public void Setup() {
		this.dfile = new File("plugins/FactionWarps", "factions.yml");
		if (!this.dfile.exists()) {
			try {
				this.dfile.createNewFile();
			} catch (IOException e) {
				Bukkit.getServer().getLogger().info("FactionWarps >> Setup file exception!");
			}
		}
		Bukkit.getServer().getLogger().info("FactionWarps >> Loading factions.yml!");
		this.data = YamlConfiguration.loadConfiguration(this.dfile);
		Bukkit.getServer().getLogger().info("FactionWarps >> factions.yml loaded!");
	}

	public FileConfiguration GetData() {
		return this.data;
	}

	public void SaveData() {
		try {
			if (this.dfile == null || this.data == null) {
				return;
			}
			this.data.save(this.dfile);
		} catch (IOException e) {
			Bukkit.getServer().getLogger().info("FactionWarps >> Save file exception!");
		}
	}

	public void ReloadData() {
		this.data = YamlConfiguration.loadConfiguration(this.dfile);
	}
}