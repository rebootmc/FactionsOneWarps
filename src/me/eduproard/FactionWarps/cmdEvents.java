/**
 *
 * FactionWarps - Copyright 2016 P.Eduard All Rights Reserved
 */
package me.eduproard.FactionWarps;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Rel;

import net.md_5.bungee.api.ChatColor;

public class cmdEvents implements Listener {
	
	private Main Main;
	public cmdEvents(Main Plugin) {
		Main = Plugin;

		Main.getServer().getPluginManager().registerEvents(this, Main);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void on(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		String Label = e.getMessage();
		String[] args = Label.split(" ");
		if(e.getMessage().toLowerCase().contains("/f warp create") || e.getMessage().toLowerCase().contains("/f warp add") || e.getMessage().toLowerCase().contains("/f warp set")) {
			e.setCancelled(true);
			if(p.hasPermission("factionwarps.create")) {
				if(args.length == 3 || args.length >= 6) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX-ERROR")));
					return;
				}
				if(args.length == 4 || args.length == 5) {
					String WarpName = args[3].toLowerCase();
					
					if(WarpName.equalsIgnoreCase("add") || WarpName.equalsIgnoreCase("create") || WarpName.equalsIgnoreCase("set") || WarpName.equalsIgnoreCase("list") || WarpName.equalsIgnoreCase("remove")) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX-ERROR")));						
						return;
					}
					
					FileUtils Utils = new FileUtils(Main);
					FPlayer Player = FPlayers.i.get(p);
					boolean MustBeInTerritory = Main.getConfig().getBoolean("mustBeInTerritory");
					double Cost = Main.getConfig().getDouble("WarpCost");
					
					if(Main.ConfirmationList.contains(p.getName())) {
						String Password = null;
						
						if(args.length == 5) {
							Password = args[4];
						}
						
						Location Location = p.getLocation();
						
						if(Main.getVault().getBalance(p.getName()) < Cost) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-MONEY").replaceAll("%money%", Cost + "")));
							return;
						}
						
						if(MustBeInTerritory == true) {
							Faction Faction = Board.getFactionAt(Location);
							if(isValidFaction(Faction) == true) {
								if(Faction.getTag().equals(Player.getFaction().getTag())) {
									Utils.AddWarp(Player.getFaction().getTag(), WarpName, Location, Password);
									Main.getVault().withdrawPlayer(p.getName(), Cost);
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WARP-SET")));
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("MUST-BE-IN-YOUR-TERRITORY")));
								}
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("MUST-BE-IN-YOUR-TERRITORY")));
							}
						} else {
							Utils.AddWarp(Player.getFaction().getTag(), WarpName, Location, Password);
							Main.getVault().withdrawPlayer(p.getName(), Cost);
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WARP-SET")));
						}
						Main.ConfirmationList.remove(p.getName());
						return;
					}
					
					String MinimumManager = Main.getConfig().getString("minimumManage");
					
					if(Main.GetStorage().GetData().contains("Factions." + Player.getFaction().getTag())) {
						int Size = Main.GetStorage().GetData().getConfigurationSection("Factions." + Player.getFaction().getTag()).getKeys(false).size();
						int MaximumWarps = Main.getConfig().getInt("maximumWarps");
						if((Size + 1) > MaximumWarps) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WARPS-LIMIT")));
							return;
						}
					}
					if(Player.getFaction() == null || Player.getFaction().isNone() == true) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-FACTION")));
						return;
					}
					if(Player.getRole().isAtLeast(Rel.parse(MinimumManager)) == false) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NOT-ALLOWED-MANAGE")));
						return;
					}
					
					if(Main.getVault().getBalance(p.getName()) < Cost) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-MONEY").replaceAll("%money%", Cost + "")));
						return;
					}
					
					Location Location = p.getLocation();
					if(MustBeInTerritory == true) {
						Faction Faction = Board.getFactionAt(Location);
						if(isValidFaction(Faction) == true) {
							if(Faction.getTag().equals(Player.getFaction().getTag())) {
								
								Main.ConfirmationList.add(p.getName());
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("CREATE-CONFIRM").replaceAll("%money%", Cost + "")));
								
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("MUST-BE-IN-YOUR-TERRITORY")));
								return;
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("MUST-BE-IN-YOUR-TERRITORY")));
						}
					} else {
						Main.ConfirmationList.add(p.getName());
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("CREATE-CONFIRM").replaceAll("%money%", Cost + "")));
					}
					return;
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-PERMISSION")));
			}
		} else if(e.getMessage().toLowerCase().contains("/f warp remove")) {
			e.setCancelled(true);
			if(p.hasPermission("factionwarps.remove")) {
			if(args.length == 3 || args.length >= 6) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX-ERROR")));
				return;
			}
			if(args.length == 4 || args.length == 5) {
				String WarpName = args[3].toLowerCase();
				
				String MinimumManager = Main.getConfig().getString("minimumManage");
				FPlayer Player = FPlayers.i.get(p);
				
				if(Player.getFaction() == null || Player.getFaction().isNone() == true) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-FACTION")));
					return;
				}
				
				if(Player.getRole().isAtLeast(Rel.parse(MinimumManager)) == false) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NOT-ALLOWED-MANAGE")));
					return;
				}
				FileUtils Utils = new FileUtils(Main);
				if(Utils.ExistWarp(Player.getFaction().getTag(), WarpName) == false) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-WARP")));
					return;
				}
				
				if(Utils.HasPassword(Player.getFaction().getTag(), WarpName) == true) {
					if(args.length == 5) {
						String Password = args[4];
						String WarpPassword = Utils.GetWarpPassword(Player.getFaction().getTag(), WarpName);
						if(Password.equals(WarpPassword) == false) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WRONG-PASSWORD")));
							return;
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("PASSWORD-REQUIRED")));
						return;
					}
				}
				Utils.RemoveWarp(Player.getFaction().getTag(), WarpName);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WARP-REMOVE")));
				return;
			}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-PERMISSION")));
			}
		} else if(e.getMessage().toLowerCase().contains("/f warp list")) {
			e.setCancelled(true);
			if(p.hasPermission("factionwarps.list")) {
				if(args.length >= 4) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX-ERROR")));
					return;
				}
				if(args.length == 3) {
					String MinimumManager = Main.getConfig().getString("minimumList");
					FPlayer Player = FPlayers.i.get(p);
					
					if(Player.getFaction() == null || Player.getFaction().isNone() == true) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-FACTION")));
						return;
					}

					if(Player.getRole().isAtLeast(Rel.parse(MinimumManager)) == false) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NOT-ALLOWED-LIST")));
						return;
					}
					
					if(Main.GetStorage().GetData().contains("Factions." + Player.getFaction().getTag()) == false) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-WARPS")));
						return;
					}
					
					StringBuilder list = new StringBuilder("");
					
					int Size = Main.GetStorage().GetData().getConfigurationSection("Factions." + Player.getFaction().getTag()).getKeys(false).size();
					int Point = 0;
					for(String Warp : Main.GetStorage().GetData().getConfigurationSection("Factions." + Player.getFaction().getTag()).getKeys(false)) {
						if((Size - Point) == 1) {
							list.append(Warp + ".");
						} else {
							list.append(Warp + ", ");
							Point++;
						}
					}
					String CompleteString = ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WARP-LIST").replaceAll("%warplist%", list.toString()));
					p.sendMessage(CompleteString);
					return;
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-PERMISSION")));
			}
		} else if(e.getMessage().toLowerCase().contains("/f warp")) {
			e.setCancelled(true);
			if(p.hasPermission("factionwarps.warp")) {
				if(args.length == 2 || args.length >= 5) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX-ERROR")));
					return;
				}
				if(args.length == 3 || args.length == 4) {
					String WarpName = args[2].toLowerCase();
					
					String MinimumUse = Main.getConfig().getString("minimumUse");
					double DistanceFromEnemiesMinimum = Main.getConfig().getDouble("distanceFromEnemiesMinimum");
					boolean DistanceFromEnemiesIgnoreInOwn = Main.getConfig().getBoolean("distanceFromEnemiesIgnoreInOwn");
					boolean SecondsLenient = Main.getConfig().getBoolean("secondsLenient");
					int SecondsCooldown = Main.getConfig().getInt("secondsCooldown");
					int SecondsWarmup = Main.getConfig().getInt("secondsWarmup");
					
					FPlayer Player = FPlayers.i.get(p);
					if(Player.getFaction() == null || Player.getFaction().isNone() == true) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-FACTION")));
						return;
					}
					Faction FactionPlayer = Player.getFaction();
					Location Location = p.getLocation();
					Faction FactionInside = Board.getFactionAt(Location);
					FileUtils Utils = new FileUtils(Main);
					
					if(Utils.ExistWarp(FactionPlayer.getTag(), WarpName) == false) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-WARP")));
						return;
					}
					Location WarpLocation = Utils.GetWarpLocation(FactionPlayer.getTag(), WarpName);
					Faction WarpFaction = Board.getFactionAt(WarpLocation);
					
					if(WarpFaction == null || WarpFaction.isNone() == true) {
						Utils.RemoveWarp(FactionPlayer.getTag(), WarpName);
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-YOUR-WARP")));
						return;
					}
					
					if(Utils.HasPassword(FactionPlayer.getTag(), WarpName) == true) {
						if(args.length == 4) {
							String Password = args[3];
							String WarpPassword = Utils.GetWarpPassword(FactionPlayer.getTag(), WarpName);
							if(Password.equals(WarpPassword) == false) {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("WRONG-PASSWORD")));
								return;
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("PASSWORD-REQUIRED")));
							return;
						}
					}
					
					if(FactionPlayer.getTag().equals(WarpFaction.getTag()) == false) {
						Utils.RemoveWarp(FactionPlayer.getTag(), WarpName);
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-YOUR-WARP")));
					}
					
					if(p.hasPermission("factionwarps.cooldown.bypass") == false) {
						if(SecondsCooldown > 0) {
							if(Main.CooldownMap.containsKey(p.getName()) == true) {
								long OldTime = Main.CooldownMap.get(p.getName());
								long NewTime = System.currentTimeMillis();
								long Comparison = SecondsCooldown - ((NewTime - OldTime) / 1000);
								if(Comparison > 0L) {
									if(SecondsLenient == false || Comparison > (long)0.80) {
										p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("COOLDOWN").replaceAll("%cooldown%", Comparison + "")));
										return;
									}
								}
							}
							Main.CooldownMap.put(p.getName(), System.currentTimeMillis());
						}
					}
					
					if(Player.getRole().isAtLeast(Rel.parse(MinimumUse)) == false) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NOT-ALLOWED-USE")));
						return;
					}
					
					boolean EnemyNear = false;
					if(DistanceFromEnemiesMinimum > 0) {
						for (Player player : p.getWorld().getPlayers()) {
							if (player.getLocation().distance(p.getLocation()) <= DistanceFromEnemiesMinimum) {
								FPlayer otherPlayer = FPlayers.i.get(player);
								if (otherPlayer.getRelationTo(Player) == Rel.ENEMY) {
									if(FactionInside.getTag().equals(FactionPlayer.getTag()) == false) {
										p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("DISTANCE-FROM-ENEMIES")));
										return;
									}
									EnemyNear = true;
								}
							}
						}
					}
					if(EnemyNear == true) {
						if(DistanceFromEnemiesIgnoreInOwn == true) {
							TeleportPlayer(p, WarpLocation, WarpName, 20*SecondsWarmup);
							return;
						}
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("DISTANCE-FROM-ENEMIES")));
						return;
					}
					TeleportPlayer(p, WarpLocation, WarpName, 20*SecondsWarmup);
					return;
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO-PERMISSION")));
			}
		}
	}
	
	private void TeleportPlayer(Player p, Location t, String w, int s) {
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("TELEPORTING-WARP").replaceAll("%warpname%", w)));
		BukkitTask Task = new BukkitRunnable() {
			private Player Player;
			private Location Location;
			private String WarpName;
			
			public BukkitRunnable set(Player _Player, Location _Location, String _WarpName) {
				Player = _Player;
				Location = _Location;
				WarpName = _WarpName;
				return this;
			}
			
			@Override
			public void run() {
				Player.teleport(Location);
				Player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("TELEPORTED-WARP").replaceAll("%warpname%", WarpName)));
				Main.WarmupMap.remove(Player.getName());
			}
		}.set(p, t, w).runTaskLater(Main, s);
		Main.WarmupMap.put(p.getPlayer().getName(), Task);
	}
	
	private boolean isValidFaction(Faction faction) {
		if (faction.isNone() == false && faction.getTag().equals("SafeZone") == false && faction.getTag().equals("WarZone") == false) {
			return true;
		}
		return false;
	}
}