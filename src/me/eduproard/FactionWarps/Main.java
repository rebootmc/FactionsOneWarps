/**
 *
 * FactionWarps - Copyright 2016 P.Eduard All Rights Reserved
 */
package me.eduproard.FactionWarps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {
	
	public Map<String, Long> CooldownMap = new HashMap<>();
	public Map<String, BukkitTask> WarmupMap = new HashMap<>();
	public List<String> ConfirmationList = new ArrayList<>();
	private FactionStorage FactionStorage;
	private Economy VaultPlugin;
	
	public void onEnable() {
		boolean Faction = FACTION();
		RegisteredServiceProvider<Economy> Provider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
		if(Provider != null) {
			VaultPlugin = Provider.getProvider();
		}
		if(VaultPlugin == null) {
			Bukkit.getServer().getLogger().info("FactionWarps >> Vault is not installed!");
		}
		saveDefaultConfig();
		FactionStorage = new FactionStorage();
		FactionStorage.Setup();
		FactionStorage.SaveData();
		
		new cmdEvents(this);
		new envEvents(this);
		
		getLogger().info("ENABLED! Faction Status: " + Faction + ".");
	}
	
	public FactionStorage GetStorage() {
		return FactionStorage;
	}
	public Economy getVault() {
		return VaultPlugin;
	}
	private boolean FACTION() {
		return Bukkit.getPluginManager().isPluginEnabled("Factions");
	}
}