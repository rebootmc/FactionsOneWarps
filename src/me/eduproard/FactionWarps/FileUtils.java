/**
 *
 * FactionWarps - Copyright 2016 P.Eduard All Rights Reserved
 */
package me.eduproard.FactionWarps;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class FileUtils {
	
	private Main Main;
	public FileUtils(Main Plugin) {
		Main = Plugin;
	}
	public boolean ExistWarp(String Faction, String WarpName) {
		return Main.GetStorage().GetData().contains("Factions." + Faction + "." + WarpName);
	}
	public boolean HasPassword(String Faction, String WarpName) {
		return Main.GetStorage().GetData().contains("Factions." + Faction + "." + WarpName + ".Password");
	}
	public Location GetWarpLocation(String Faction, String WarpName) {
		if(ExistWarp(Faction, WarpName)) {
			return StringToLocation(Main.GetStorage().GetData().getString("Factions." + Faction + "." + WarpName + ".Location"), true);
		}
		return null;
	}
	public String GetWarpPassword(String Faction, String WarpName) {
		if(HasPassword(Faction, WarpName)) {
			return Main.GetStorage().GetData().getString("Factions." + Faction + "." + WarpName + ".Password");
		}
		return null;
	}
	public void AddWarp(String Faction, String WarpName, Location Location, String Password) {
		Main.GetStorage().GetData().set("Factions." + Faction + "." + WarpName + ".Location", LocationToString(Location, true));
		Main.GetStorage().GetData().set("Factions." + Faction + "." + WarpName + ".Password", Password);
		Main.GetStorage().SaveData();
	}
	public void RemoveWarp(String Faction, String WarpName) {
		Main.GetStorage().GetData().set("Factions." + Faction + "." + WarpName + ".Location", null);
		Main.GetStorage().GetData().set("Factions." + Faction + "." + WarpName + ".Password", null);
		Main.GetStorage().GetData().set("Factions." + Faction + "." + WarpName, null);
		Main.GetStorage().SaveData();
		if(Main.GetStorage().GetData().getConfigurationSection("Factions." + Faction).getKeys(false).isEmpty() == true) {
			Main.GetStorage().GetData().set("Factions." + Faction, null);
			Main.GetStorage().SaveData();
		}
	}
	
	
	private String LocationToString(Location Location, boolean YawPitch) {
		if(Location == null) {
			return null;
		}
		World W = Location.getWorld();
		int X = Location.getBlockX();
		int Y = Location.getBlockY();
		int Z = Location.getBlockZ();
		float YAW = Location.getYaw();
		float PITCH = Location.getPitch();
		
		StringBuilder Converted = new StringBuilder();
		Converted.append(W.getName() + "&" + X + "&" + Y + "&" + Z);
		if(YawPitch == true) {
			Converted.append("&" + YAW + "&" + PITCH);
		}
		return Converted.toString();
	}
	private Location StringToLocation(String Location, boolean YawPitch) {
		if(Location == null) {
			return null;
		}
		String[] Splitted = Location.split("&");
		
		World W = Bukkit.getWorld(Splitted[0]);
		int X = Integer.parseInt(Splitted[1]);
		int Y = Integer.parseInt(Splitted[2]);
		int Z = Integer.parseInt(Splitted[3]);
		float YAW;
		float PITCH;
		if(YawPitch == true) {
			YAW = Float.parseFloat(Splitted[4]);
			PITCH = Float.parseFloat(Splitted[5]);
			
			return new Location(W, X, Y, Z, YAW, PITCH);
		}
		return new Location(W, X, Y, Z);
		
	}
}